source 'https://rubygems.org'

## Core knihovny
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0.rc1'
# Use mysql as the database for Active Record
gem 'mysql2'
# Použijme pro ukládání session redis
gem 'redis-session-store'


## Assets
# Rails less
gem 'less-rails'
# Twitter bootstrap https://github.com/metaskills/less-rails-bootstrap
gem 'less-rails-bootstrap'
# Font Awesome icons
gem "font-awesome-rails"
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer',  platforms: :ruby
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'

## API
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'


## Systémové knihovny
# bundle exec rake doc:rails generates the API under doc/api.
# gem 'sdoc', '~> 0.4.0',          group: :doc

group :development do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # gem 'spring'

  # https://github.com/charliesome/better_errors
  gem "better_errors"
  gem 'binding_of_caller'

  # Annotate models https://github.com/ctran/annotate_models
  gem 'annotate'

  # https://github.com/myronmarston/mail_safe
  gem "mail_safe"

  gem "bullet"
end

group :test do
  # mocking for testing
  gem 'mocha', group: :test

  # fixture library
  gem "factory_girl_rails", "~> 4.0"
  gem "factory_girl", "~> 4.0"
end

group :production do
  gem 'unicorn'
end

# Lepší konzole https://github.com/pry/pry
# TODO Chtělo by to group 'console' aby se neloadoval v produkci na serveru, ale v produkční konzoli ano.
gem 'pry-rails'

## Asynchronní zpracování
# Sidekiq http://sidekiq.org/
gem 'sidekiq'
# Sidekiq middleware, který dokáže limitovat na kolika threadech/proceseh běží jaká fronta https://github.com/brainopia/sidekiq-limit_fetch
# gem 'sidekiq-limit_fetch'
# Sinatra pro mount aplikace pro sidekiq UI
gem 'sinatra', '>= 1.3.0', :require => nil

# https://github.com/intridea/oauth2
gem 'oauth2'

# Attachment gem https://github.com/thoughtbot/paperclip
gem 'paperclip'

# https://github.com/apotonick/cells
gem 'cells'

# Soft deleting https://github.com/radar/paranoia
gem "paranoia", ">= 2.0"

# Application config
gem 'settingslogic'

# Detekce timezone dle lat a lng https://github.com/panthomakos/timezone
# Používá http://www.geonames.org/
# http://www.geonames.org/login
# 6artisans/sixartisan
# Limit 2000/hodinu
#       30000/den
gem 'timezone'

# NewRelic
gem 'newrelic_rpm'

# EM-HTTP-Request - paralelní stahování - https://github.com/igrigorik/em-http-request
# Má to problém, že umíme stahovat pouze v jednom vláknu sidekiqu :/
# gem 'em-http-request'


# https://github.com/fmmfonseca/work_queue
gem 'work_queue'
