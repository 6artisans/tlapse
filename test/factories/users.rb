# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    remote_id 1
    token_hash "MyString"
  end
end
