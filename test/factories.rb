FactoryGirl.define do
  factory :long_video_without_filters, class: Schedule do
    start_at "2017-07-01 08:00:00"
    stop_at  "2017-07-09 19:00:00"
    timezone "Europe/Prague"
    camera_id 1
    filters []
  end

  factory :medium_video_with_filters, class: Schedule do
    start_at "2017-02-27 06:00:00"
    stop_at  "2017-03-01 19:00:00"
    timezone "US/Mountain"
    camera_id 1
    filters [Filter.new(from: "06:00", to: "12:00"), Filter.new(from: "13:00", to: "19:30")]
  end
end