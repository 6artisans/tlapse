require 'test_helper'

class RecordingTest < ActiveSupport::TestCase
  test "new instance has the newest VideoSettings" do
    r = create_recording
    assert_equal 1440, r.max_frames
    assert_equal 10, r.min_period
  end

  test "finding of current schedule" do
    r = create_recording
    create_schedule(r, start_at: Time.now + 1.day, canceled_at: Time.now + 10.minutes)
    create_schedule(r, start_at: Time.now + 2.day, canceled_at: Time.now + 15.minutes)
    s = create_schedule(r, start_at: Time.now + 3.day)

    assert_equal r.current_schedule, s
  end

  test "choose final video" do
    r = create_recording_with_videos

    assert r.choose_final_video!(1)
    assert_equal r.video_id, 1
  end

  test "can not choose video from other recording" do
    r = create_recording_with_videos

    assert_not r.choose_final_video!(100)
    assert_nil r.video_id
  end

  test "start recording increase attempts and create snapshotters" do
    r = create_recording
    s = create_schedule(r, start_at: Time.now + 1.day, canceled_at: Time.now + 10.minutes)

    Daemons::Planner.expects(:create_snapshotters!).times(1)
    r.start!
    assert_equal r.attempts, 2
  end

  def create_schedule recording, start_at: nil, stop_at: Time.now + 1.week, canceled_at: nil
    recording.schedules.create(start_at: start_at,
                               stop_at: stop_at,
                               timezone: "Europe/Prague",
                               camera_id: 1,
                               recording: recording,
                               filters: [],
                               canceled_at: canceled_at)
  end

  def create_recording_with_videos
    r = create_recording
    r.stubs(:current_schedule).returns(Schedule.new)
    Schedule.any_instance.stubs(:videos).returns([Video.new(id: 1),
                                                  Video.new(id: 2)])
    return r
  end

  def create_recording
    Recording.create(name: "Bathroom camera", attempts: 1, user_id: 1)
  end

end
