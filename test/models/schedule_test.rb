require 'test_helper'

class ScheduleTest < ActiveSupport::TestCase
  setup do
    Click2Stream::Apiable.skip_api_calls = true
  end

  test "set start_at with start_at_hash=" do
    schedule = Schedule.new(start_at_hash: {date: "2014/08/06", hour: "11", minute: "55", ampm: "AM"})
    assert_equal schedule.start_at, Time.parse("2014-08-06 11:55:00 UTC")
  end

  test "calculates number of frames to make for sparse lapse" do

    s = build(:long_video_without_filters, recording: mock_recording(2000, 10))
    s.calculate_frames!
    assert_equal(2000, s.frames)
  end

  test "calculates number of frames to make for dense lapse" do
    s = build(:medium_video_with_filters, recording: mock_recording(16000, 9))
    s.calculate_frames!
    assert_equal(14800, s.frames)
  end

  test "schedule without filters creates single snapshotter" do
    assert_equal(
      [["2017-07-01 06:00:00", "2017-07-09 17:00:00"]],
      schedule_to_snapshotters(attributes_for :long_video_without_filters)
    )
  end

  test "schedule with filters creates multiple snapshotters" do
    assert_equal(
      [
        ["2017-02-27 13:00:00", "2017-02-27 19:00:00"],
        ["2017-02-27 20:00:00", "2017-02-28 02:30:00"],
        ["2017-02-28 13:00:00", "2017-02-28 19:00:00"],
        ["2017-02-28 20:00:00", "2017-03-01 02:30:00"],
        ["2017-03-01 13:00:00", "2017-03-01 19:00:00"],
        ["2017-03-01 20:00:00", "2017-03-02 02:00:00"]],
      schedule_to_snapshotters(attributes_for :medium_video_with_filters)
    )
  end

  test "validates correct format of dates" do
    s = Schedule.new(start_at: "2017-02-34 13:00:00", stop_at: "2017-03-01 13:00:00")
    assert_validation s, start_at: /correct datetime format/
  end

  test "validates both dates in future" do
    s = Schedule.new(start_at: "0000-02-28 13:00:00", stop_at: "2013-03-01 13:00:00")
    assert_validation s, start_at: /future/, stop_at: /future/
  end

  test "validates dates must be consecutive" do
    s = Schedule.new(start_at: "2017-02-22 13:00:00", stop_at: "2017-01-01 13:00:00")
    assert_validation s, stop_at: /after/
  end

  test "videos created returns true when all videos has been generated" do
    videos = [
      Video.new(finished_at: "2017-02-22 13:00:00"),
      Video.new(finished_at: "2017-02-22 13:00:00"),
      Video.new(finished_at: "2017-02-22 13:00:00")
    ]

    s = build(:long_video_without_filters, videos: videos)
    assert s.videos_created?
  end

  test "videos created returns false when any videos exist" do
    s = build(:long_video_without_filters)
    s.stubs(:videos).returns([])
    assert_not s.videos_created?
  end

  test "videos created returns false when one of videos has not been generated" do
    videos = [
      Video.new(finished_at: "2017-02-22 13:00:00"),
      Video.new(finished_at: nil),
      Video.new(finished_at: "2017-02-22 13:00:00")
    ]

    s = build(:long_video_without_filters, videos: videos)
    assert_not s.videos_created?
  end

  def mock_recording(max_frames, min_period)
    user = User.new(id:1)
    user.stubs(:camera).returns(Camera.new(shortname: "Testovací kamera"))

    recording = Recording.new(user: user,
                              video_settings: VideoSettings.new(max_frames: max_frames,
                                                                min_period: min_period))

    recording
  end

  def schedule_to_snapshotters(args)
    s = Schedule.new(args.merge(recording: mock_recording(1440, 10)))

    s.create_snapshotters!

    s.snapshotters.map { |s|
      [s.start_at.to_s(:db), s.stop_at.to_s(:db)]
    }
  end
end
