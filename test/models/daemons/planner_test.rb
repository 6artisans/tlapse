require 'test_helper'

class PlannerTest < ActiveSupport::TestCase
  test "create_schedule! creates a delayed job" do
    calls_background_job(Daemons::SnapshotterCreator, anything)
    Daemons::SnapshotterCreator.expects(:perform)
    Daemons::Planner.create_schedule!(Recording.find(1), attributes_for(:long_video_without_filters))
  end

  test "create_snapshotters! creates snapshotters and schedules their download" do
    schedule = Schedule.find(1)

    calls_background_job(Daemons::SnapshotterCreator, schedule.id)
    Snapshotter.any_instance.expects(:create_in_api!)
    Daemons::Planner.expects(:download_images_at!).with(Time.zone.parse("2018-07-14 14:47:38"), anything)

    Daemons::Planner.create_snapshotters!(schedule)

    assert_equal 1, Schedule.first.snapshotters.count
  end

  test "download_images_at! runs download task for given snapshotter" do
    schedule = Schedule.find(2)
    time = Time.zone.now

    Snapshotter.any_instance.expects(:download_images!).returns(true)
    Daemons::Planner.expects(:create_video!).with(schedule)
    calls_delayed_job(time, Daemons::ImageDownloader, 1)

    Daemons::Planner.download_images_at!(time, schedule.snapshotters.first)
  end

  test "create_video! runs video creation in background" do
    schedule = Schedule.find(2)
    Settings.videos_durations.each do |duration|
      Video.expects(:create).with(schedule: schedule, duration: duration)
    end
    calls_background_job(Daemons::VideoCreator, 2)

    Daemons::Planner.create_video!(schedule)
  end

  # simuluje volani jobu na pozadi, volany job se spusti ihned
  def calls_background_job klass, *attrs
    Sidekiq::Client.expects(:enqueue).with(klass, *attrs) { klass.new.perform(*attrs); true }
  end

  # simuluje volani jobu v budoucnosti, volany job se spusti ihned
  def calls_delayed_job time, klass, *attrs
    Sidekiq::Client.expects(:enqueue_in).with(time, klass, *attrs) { klass.new.perform(*attrs); true }
  end
end