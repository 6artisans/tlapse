require 'test_helper'

class TokenTest < ActiveSupport::TestCase

  def setup
    @token_hash = {token_type: "Bearer",
                   scope: nil,
                   access_token: "a" * 40,
                   refresh_token: "r" * 40,
                   expires_at: (Time.now - 1.second).to_i}

    @access_token = OAuth2::AccessToken.from_hash(Auth.client, @token_hash.clone)

    @refreshed_token_hash = {token_type: "Bearer",
                             scope: nil,
                             access_token: "b" * 40,
                             refresh_token: "r" * 40,
                             expires_at: (Time.now + 60.minute).to_i}

    @refeshed_access_token = OAuth2::AccessToken.from_hash(Auth.client, @refreshed_token_hash.clone)

    OAuth2::AccessToken.any_instance.stubs(:refresh!).returns(@refeshed_access_token)
    Auth::Token.any_instance.stubs(:user).returns(User.new)
  end

  test "making new auth token from hash" do
    @auth_token = Auth::Token.new(@token_hash.clone)

    assert_equal @auth_token.token.to_hash, @token_hash
  end

  test "making new auth token from OAuth2::AccessToken instance" do
    @auth_token = Auth::Token.new(@access_token.clone)

    assert_equal @auth_token.token.to_hash, @access_token.to_hash
  end

  test "check of expired token make new one" do
    @auth_token = Auth::Token.new(@access_token.clone)
    @auth_token.check_expiry!

    assert_equal @auth_token.token.to_hash, @refreshed_token_hash
  end
end
