require 'test_helper'

class TokenTest < ActiveSupport::TestCase

  setup do
    @token_hash = {token_type: "Bearer",
                   scope: nil,
                   access_token: "a" * 40,
                   refresh_token: "r" * 40,
                   expires_at: (Time.now - 1.second).to_i}

    @c2s_user_hash = {id: "4741",
                      firstname: nil,
                      surname: nil,
                      address_city: nil,
                      created_datetime: "2014-04-04 13:45:21",
                      email: "honza@6artisans.cz",
                      company: nil,
                      address_street1: nil,
                      address_street2: nil,
                      address_zip: nil}

    @c2s_user_ostruct = OpenStruct.new(@c2s_user_hash.clone)

    @auth_token = Auth::Token.new(@token_hash.clone)

    Click2Stream::Api.any_instance.stubs(:get).returns(@c2s_user_hash.clone)
  end

  test "find existing user with token" do
    @user = User.create(remote_id: @c2s_user_ostruct.id, remote_data: @c2s_user_ostruct, token: @auth_token)

    assert_equal @user, User.find_or_create_by_token(@auth_token)
  end

  test "create new user with token" do
    user = User.find_or_create_by_token(@auth_token)

    assert_equal user.remote_id, @c2s_user_ostruct.id.to_i
    assert_equal user.remote_data, @c2s_user_ostruct
  end
end
