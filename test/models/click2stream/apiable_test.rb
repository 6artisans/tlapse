require 'test_helper'

class ApiStrings
  include Click2Stream::Apiable
  server "http://apistrings.cz"
end

class ApiLambdas
  include Click2Stream::Apiable
  server ->() { "http://apilambdas.cz" }
end

class ApiSymbols
  include Click2Stream::Apiable
  server :server

  def server
    "http://apisymbols.cz"
  end
end

class ApiableTest < ActiveSupport::TestCase

  def expect(method, url, payload)
    Auth::Token.expects(method.to_sym).
                        with(url, body: payload.to_json,
                                  headers: {
                                    content_type: "application/json",
                                    accept: "application/json"
                                  })
  end

  setup do
    @token = Auth::Token.new({token_type: "Bearer",
                              scope: nil,
                              access_token: "a" * 40,
                              refresh_token: "r" * 40,
                              expires_at: (Time.now + 1.hour).to_i})

    # Marshaled OAuth2::Response. Nevím jak jinak to nafejkovat :)
    @response = Marshal.load("\x04\bo:\x15OAuth2::Response\a:\x0E@responseU:\x16Faraday::Response{\b:\vstatusi\x01\xC8:\tbodyI\"\x02\x19\x02[{\"id\":\"4412\",\"url\":\"rtsp:\\/\\/viewer:bozka@upgm-ipkam2.fit.vutbr.cz\\/axis-media\\/media.amp?videocodec=h264&streamprofile=Bandwidth\",\"shortname\":\"Honzova superkamera\",\"coords\":\"49.2, 16.6333\",\"user_id\":\"4741\",\"type\":\"h264\",\"setup\":\"common\",\"created_datetime\":\"2014-04-04 15:17:56\",\"timezone\":\"-8.0\"},{\"id\":\"4841\",\"url\":\"rtsp:\\/\\/uplnecokoliv.testcam2.click2stream.com\\/live.sdp\",\"shortname\":\"535f612e8dd30\",\"coords\":\"49.75, 15.5\",\"user_id\":\"4741\",\"type\":\"h264\",\"setup\":\"common\",\"created_datetime\":\"2014-04-29 10:22:22\",\"timezone\":\"-8.0\"}]\x06:\x06ET:\x15response_headersIC:\x1CFaraday::Utils::Headers{\x0FI\"\tdate\x06;\nTI\"\"Tue, 05 Aug 2014 16:30:10 GMT\x06;\nTI\"\vserver\x06;\nTI\"\x12Apache/2.2.15\x06;\nTI\"\x11x-powered-by\x06;\nTI\"\x14Nette Framework\x06;\nTI\"\x0Fset-cookie\x06;\nTI\"\x01\x9APHPSESSID=vmf4qf6lq9tt12pg0lq8j55716; path=/; HttpOnly, PHPSESSID=vmf4qf6lq9tt12pg0lq8j55716; path=/; httponly, nette-browser=1n9efnnye9; path=/; httponly\x06;\nTI\"\fexpires\x06;\nTI\"\"Mon, 23 Jan 1978 10:00:00 GMT\x06;\nTI\"\x12cache-control\x06;\nTI\"+s-maxage=0, max-age=0, must-revalidate\x06;\nTI\"\vpragma\x06;\nTI\"\rno-cache\x06;\nTI\"\x13content-length\x06;\nTI\"\b537\x06;\nTI\"\x0Fconnection\x06;\nTI\"\nclose\x06;\nTI\"\x11content-type\x06;\nTI\"\x15application/json\x06;\nT\x06:\v@names{\x0FI\"\tdate\x06;\nT@\nI\"\vserver\x06;\nT@\fI\"\x11x-powered-by\x06;\nT@\x0EI\"\x0Fset-cookie\x06;\nT@\x10I\"\fexpires\x06;\nT@\x12I\"\x12cache-control\x06;\nT@\x14I\"\vpragma\x06;\nT@\x16I\"\x13content-length\x06;\nT@\x18I\"\x0Fconnection\x06;\nT@\x1AI\"\x11content-type\x06;\nT@\x1C:\r@options{\x06:\nparse0")

    OAuth2::AccessToken.any_instance.stubs(:get).returns(@response.clone)
    OAuth2::AccessToken.any_instance.stubs(:post).returns(@response.clone)
    OAuth2::AccessToken.any_instance.stubs(:delete).returns(@response.clone)

    Click2Stream::Apiable.skip_api_calls = false
  end

  test "allows set variables as strings" do
    expect(:delete, "http://apistrings.cz/apistrings", {})
    ApiStrings.new(@token).delete("/apistrings")
  end

  test "allows set variables as lambdas" do
    expect(:post, "http://apilambdas.cz/apilambdas", {})
    ApiLambdas.new(@token).post("/apilambdas")
  end

  test "allows set variables as symbols" do
    expect(:get, "http://apisymbols.cz/apisymbols", {})
    ApiSymbols.new(@token).get("/apisymbols")
  end

end
