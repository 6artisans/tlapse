require 'test_helper'

class VideoTest < ActiveSupport::TestCase

  def setup
    @video = Video.new(schedule_id: 1, duration: 60)
    @video.stubs(:schedule).returns(Schedule.new)
    Schedule.any_instance.stubs(:recording).returns(Recording.new(name: "Bathroom camera", user_id: 1))
    Schedule.any_instance.stubs(:real_frames_count).returns(1440)
  end

  test "before create makes path" do
    @video.save
    assert_not_nil @video.path
  end

  test "making correct video path" do
    @video.save
    assert_equal "#{Settings.videos_directory}/1/bathroom-camera-60.mp4", @video.path
  end

  test "before create makes image rate" do
    @video.save
    assert_not_nil @video.image_fps
  end

  test "correct video name" do
    assert_equal @video.name, "bathroom-camera-60"
  end

  test "making correct image rate" do
    @video.save
    assert_equal @video.image_fps, 24 # 1440/60
  end

  test "before create makes video rate" do
    @video.save
    assert_not_nil @video.video_fps
  end

  test "making correct video rate" do
    @video.save
    assert_equal @video.video_fps, 24 # 1440/60
  end

  test "after create generates video file" do
    Transcoder.expects(:images_to_video).times(1).returns(true)
    @video.save
  end

  test "after video is generated label video as finished" do
    Transcoder.stubs(:images_to_video).returns(true)
    @video.save
    assert_not_nil @video.finished_at
  end

end
