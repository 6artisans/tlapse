# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141103091819) do

  create_table "filters", force: true do |t|
    t.time     "from"
    t.time     "to"
    t.integer  "schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "filters", ["deleted_at"], name: "index_filters_on_deleted_at", using: :btree

  create_table "recordings", force: true do |t|
    t.string   "name"
    t.integer  "attempts",             default: 0, null: false
    t.integer  "video_settings_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "final_video_id"
    t.integer  "user_id",                          null: false
    t.datetime "deleted_at"
    t.string   "preview_file_name"
    t.string   "preview_content_type"
    t.integer  "preview_file_size"
    t.datetime "preview_updated_at"
    t.integer  "video_width"
    t.integer  "video_height"
  end

  add_index "recordings", ["deleted_at"], name: "index_recordings_on_deleted_at", using: :btree
  add_index "recordings", ["user_id"], name: "index_recordings_on_user_id", using: :btree

  create_table "schedules", force: true do |t|
    t.datetime "start_at"
    t.datetime "stop_at"
    t.string   "timezone"
    t.integer  "camera_id"
    t.integer  "recording_id"
    t.datetime "scheduled_at"
    t.datetime "finished_at"
    t.datetime "canceled_at"
    t.text     "filters"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "frames"
    t.integer  "state",         limit: 1, default: 0
    t.datetime "deleted_at"
    t.string   "error_message"
  end

  add_index "schedules", ["canceled_at"], name: "index_schedules_on_canceled_at", using: :btree
  add_index "schedules", ["deleted_at"], name: "index_schedules_on_deleted_at", using: :btree
  add_index "schedules", ["finished_at"], name: "index_schedules_on_finished_at", using: :btree
  add_index "schedules", ["recording_id"], name: "index_schedules_on_recording_id", using: :btree
  add_index "schedules", ["scheduled_at"], name: "index_schedules_on_scheduled_at", using: :btree

  create_table "snapshotters", force: true do |t|
    t.datetime "start_at"
    t.datetime "stop_at"
    t.integer  "camera_id"
    t.integer  "period"
    t.integer  "remote_id"
    t.integer  "schedule_id"
    t.datetime "sent_at"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "downloaded_at"
    t.text     "api_errors"
  end

  add_index "snapshotters", ["camera_id"], name: "index_snapshotters_on_camera_id", using: :btree
  add_index "snapshotters", ["downloaded_at"], name: "index_snapshotters_on_downloaded_at", using: :btree
  add_index "snapshotters", ["remote_id"], name: "index_snapshotters_on_remote_id", using: :btree
  add_index "snapshotters", ["schedule_id"], name: "index_snapshotters_on_schedule_id", using: :btree
  add_index "snapshotters", ["sent_at"], name: "index_snapshotters_on_sent_at", using: :btree

  create_table "users", force: true do |t|
    t.integer  "remote_id"
    t.text     "remote_data"
    t.string   "token",       limit: 511
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["remote_id"], name: "index_users_on_remote_id", unique: true, using: :btree

  create_table "video_settings", force: true do |t|
    t.integer  "max_frames", null: false
    t.integer  "min_period", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "video_fps",  null: false
  end

  add_index "video_settings", ["created_at"], name: "index_video_settings_on_created_at", using: :btree

  create_table "videos", force: true do |t|
    t.integer  "schedule_id"
    t.integer  "duration",                      null: false
    t.float    "image_fps",          limit: 24, null: false
    t.float    "video_fps",          limit: 24, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "finished_at"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.datetime "deleted_at"
    t.float    "ratio",              limit: 24
  end

  add_index "videos", ["deleted_at"], name: "index_videos_on_deleted_at", using: :btree
  add_index "videos", ["finished_at"], name: "index_videos_on_finished_at", using: :btree

end
