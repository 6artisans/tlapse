class AddErrorMessageToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :error_message, :string
  end
end
