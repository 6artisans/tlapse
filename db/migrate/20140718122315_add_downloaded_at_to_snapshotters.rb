class AddDownloadedAtToSnapshotters < ActiveRecord::Migration
  def change
    add_column :snapshotters, :downloaded_at, :datetime
    add_index :snapshotters, :downloaded_at
  end
end
