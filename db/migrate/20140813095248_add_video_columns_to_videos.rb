class AddVideoColumnsToVideos < ActiveRecord::Migration
  def up
    add_attachment :videos, :video

    Video.unscoped.find_each do |video|
      file_path = File.join(Rails.root, video.path)

      if File.exists?(file_path)
        file = File.open(file_path)
        video.video = file
        video.save
      end
    end

    remove_column :videos, :path

    old_videos_path = File.join(Rails.root, Settings.videos_directory)
    if Dir.exists?(old_videos_path)
      FileUtils.rm_r(old_videos_path)
    end
  end

  def down
    add_column :videos, :path, :string

    remove_attachment :videos, :video
  end
end
