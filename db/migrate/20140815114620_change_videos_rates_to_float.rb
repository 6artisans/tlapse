class ChangeVideosRatesToFloat < ActiveRecord::Migration
  def change
    change_column :videos, :image_rate, :float
    change_column :videos, :video_rate, :float
  end
end
