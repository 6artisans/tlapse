class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.datetime :start_at
      t.datetime :stop_at
      t.string   :timezone, limit: 10
      t.integer  :camera_id
      t.references :recording
      t.datetime :scheduled_at
      t.datetime :finished_at
      t.datetime :canceled_at
      t.text     :filters

      t.timestamps
    end
    add_index :schedules, :recording_id
    add_index :schedules, :scheduled_at
    add_index :schedules, :finished_at
    add_index :schedules, :canceled_at
  end
end
