class ChangeLengthToDurationInVideos < ActiveRecord::Migration
  def change
    rename_column :videos, :length, :duration
  end
end
