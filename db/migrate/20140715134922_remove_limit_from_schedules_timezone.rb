class RemoveLimitFromSchedulesTimezone < ActiveRecord::Migration
  def change
    change_column(:schedules, :timezone, :string, limit: 255)
  end
end
