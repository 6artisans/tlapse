class AddDeletedAtToRecording < ActiveRecord::Migration
  def change
    add_column :recordings, :deleted_at, :datetime
    add_index :recordings, :deleted_at
  end
end
