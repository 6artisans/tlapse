class CreateSnapshotters < ActiveRecord::Migration
  def change
    create_table :snapshotters do |t|
      t.datetime :start_at
      t.datetime :stop_at
      t.integer  :camera_id
      t.integer  :period
      t.integer  :remote_id
      t.references :schedule
      t.datetime :sent_at
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :snapshotters, :schedule_id
    add_index :snapshotters, :remote_id
    add_index :snapshotters, :sent_at
    add_index :snapshotters, :camera_id
  end
end
