class AddVideoIdToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :video_id, :integer
  end
end
