class AddStateToSchedules < ActiveRecord::Migration
  def change
    add_column :schedules, :state, :integer, limit: 1, default: 0
  end
end
