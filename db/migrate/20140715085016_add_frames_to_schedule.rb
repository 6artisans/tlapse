class AddFramesToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :frames, :integer
  end
end
