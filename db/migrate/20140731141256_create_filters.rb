class CreateFilters < ActiveRecord::Migration
  def change
    create_table :filters do |t|
      t.time :from
      t.time :to

      t.belongs_to :schedule
      t.timestamps
    end
  end
end
