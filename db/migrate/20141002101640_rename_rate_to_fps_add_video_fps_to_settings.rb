class RenameRateToFpsAddVideoFpsToSettings < ActiveRecord::Migration
  def change
    rename_column :videos, :video_rate, :video_fps
    rename_column :videos, :image_rate, :image_fps

    add_column :video_settings, :video_fps, :integer, null: false

    change_column_null :video_settings, :max_frames, false
    change_column_null :video_settings, :min_period, false
  end
end
