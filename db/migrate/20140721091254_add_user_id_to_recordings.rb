class AddUserIdToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :user_id, :integer, null: false
    add_index :recordings, :user_id
  end
end
