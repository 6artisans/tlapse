class RecordingsRenameVideoIdToFinalVideoId < ActiveRecord::Migration
  def change
    rename_column :recordings, :video_id, :final_video_id
  end
end
