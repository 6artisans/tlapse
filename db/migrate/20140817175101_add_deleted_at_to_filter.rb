class AddDeletedAtToFilter < ActiveRecord::Migration
  def change
    add_column :filters, :deleted_at, :datetime
    add_index :filters, :deleted_at
  end
end
