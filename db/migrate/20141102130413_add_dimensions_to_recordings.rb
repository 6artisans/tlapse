class AddDimensionsToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :video_width, :integer
    add_column :recordings, :video_height, :integer
  end
end
