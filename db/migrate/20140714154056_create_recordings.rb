class CreateRecordings < ActiveRecord::Migration
  def change
    create_table :recordings do |t|
      t.string :name
      t.integer :attempts, default: 0, null: false
      t.references :video_settings
      t.timestamps
    end
  end
end
