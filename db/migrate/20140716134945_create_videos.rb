class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.references :schedule
      t.string     :path, null: false
      t.integer    :length, null: false # in seconds
      t.string     :image_rate, null: false
      t.string     :video_rate, null: false
      t.boolean    :finished, default: 0, null: false

      t.timestamps
    end
    add_index :videos, :path, unique: true
  end
end
