class ChangeVideosFinishedToFinishedAt < ActiveRecord::Migration
  def change
    remove_column :videos, :finished
    add_column :videos, :finished_at, :datetime
    add_index :videos, :finished_at
  end
end
