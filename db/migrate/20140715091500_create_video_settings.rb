class CreateVideoSettings < ActiveRecord::Migration
  def change
    create_table :video_settings do |t|
      t.integer :max_frames
      t.integer :min_period

      t.timestamps
    end
    add_index :video_settings, :created_at
  end
end
