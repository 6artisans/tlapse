class RemoveIndexFromVideosPath < ActiveRecord::Migration
  def change
    remove_index :videos, :path
  end
end
