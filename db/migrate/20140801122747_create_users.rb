class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :remote_id
      t.text    :remote_data
      t.string  :token, limit: 511

      t.timestamps
    end

    add_index :users, :remote_id, unique: true
  end
end
