class AddAttachmentPreviewToRecordings < ActiveRecord::Migration
  def self.up
    change_table :recordings do |t|
      t.attachment :preview
    end
  end

  def self.down
    remove_attachment :recordings, :preview
  end
end
