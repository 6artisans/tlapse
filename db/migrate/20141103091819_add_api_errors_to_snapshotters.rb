class AddApiErrorsToSnapshotters < ActiveRecord::Migration
  def change
    add_column :snapshotters, :api_errors, :text
  end
end
