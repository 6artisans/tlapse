module RecordingsHelper

  def recording_image_icon_class recording
    case recording.state
      when :before_start
        klass = "preparing"
        icon  = "pause"
      when :recording
        klass = "recording"
        icon  = "stop"
      when :generating
        klass = "generating"
        icon  = "cogs"
      when :error
        klass = "error"
        icon  = "exclamation-triangle"
      else
        klass = "done"
        icon  = "play"
      end

    fa_icon icon, class: "recording__image-icon recording__image-icon--#{klass}"
  end

  # Popis stavu v kterém se nahrávka nachází
  def recording_state_text recording
    state    = recording.state
    schedule = recording.current_schedule

    content_tag :div, class: "recording__state recording__state--#{recording.state}" do
      case state
      when :before_start
        I18n.t("recording.states.#{state}.html", time: distance_of_time_in_words_to_now(schedule.make_utc_time(recording.start_at))).html_safe
      when :recording, :generating
        I18n.t("recording.states.#{state}.html", time: distance_of_time_in_words_to_now(schedule.make_utc_time(recording.stop_at))).html_safe
      else
        I18n.t("recording.states.#{state}.html", error: schedule.error_message).html_safe
      end
    end
  end

  def time_input param_prefix, value, params, defaults = {}
    params ||= {}

    concat(text_field_tag("#{param_prefix}[hour]",
                          value && value.strftime("%l") || params[:hour] || defaults[0],
                          class: "form-control input-hour",
                          maxlength: 2))

    concat(" : ")

    concat(text_field_tag("#{param_prefix}[minute]",
                          value && value.strftime("%M") || params[:minute] || defaults[1],
                          class: "form-control input-minute",
                          maxlength: 2))

    concat(" ")

    select_tag "#{param_prefix}[ampm]",
               options_for_select(%w(AM PM), value && value.strftime("%p") || params[:ampm] || defaults[2]),
               class: "select-ampm form-control"
  end

  def field_has_error_class obj, prop, class_name = "has-error"
    has_error(obj, prop) ? class_name : nil
  end

  def errors_for obj, prop
    if has_error(obj, prop)
      content_tag :label, class: "control-label recording-form__error-label" do
        safe_join get_errors(obj, prop), "<br>".html_safe
      end
    end
  end

  def has_error obj, prop
    obj.errors.has_key?(prop.to_sym)
  end

  def get_errors obj, prop
    obj.errors[prop.to_sym]
  end
end
