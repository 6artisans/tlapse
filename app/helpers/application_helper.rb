module ApplicationHelper

  def hr_with_text text
    content_tag :div, class: "separator" do
      concat tag(:hr, class: "separator__hr")
      concat(content_tag(:div, class: "separator__inner") do
        content_tag :div, text, class: "separator__text"
      end)
    end
  end

  def placeholder_tag w, h
    image_tag placeholder_image(w, h)
  end

  def placeholder_image w, h
    "http://placehold.it/#{w}x#{h}"
  end

  def schedule_duration_in_words schedule
    content_tag :span, class: "recording__duration" do
      time_ago_in_words(Time.now + schedule.duration) + " duration"
    end
  end

  def link_to_void text = nil, *params, &block
    link_to(text, "javascript:void(0)", *params, &block)
  end

  def alerts
    content_tag :div do
      flash.each do |key, msg|
        concat content_tag(:div, msg, :class => "alert alert-#{_alert_class_name(key)}")
      end
    end
  end

  def _alert_class_name flash_key
    case flash_key.to_s
    when "error"
      "danger"
    else
      flash_key
    end
  end
end
