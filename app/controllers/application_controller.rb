class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user

  before_filter :require_current_user

  private

  def current_user
    return false unless session[:user_id]
    @current_user ||= User.find(session[:user_id])
  end

  def require_current_user
    render text: "Nejsi uživatel!" unless current_user
  end
end
