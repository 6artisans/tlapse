class AuthsController < ApplicationController

  protect_from_forgery only: []

  skip_before_filter :require_current_user, except: [:logout]

  def new
    # Uložíme si kam se má uživatel redirectnout po ověření
    session[:return_back] = params[:back].present? ? params[:back] : request.referer

    redirect_to Auth.client.auth_code.authorize_url(redirect_uri: Settings.click2stream_oauth.redirect_uri,
                                                    state:        "foobar") # Nevím proč, ale c2s vyžadují aby state parametr byl v requestu
  end

  def callback
    if params[:code]
      token = Auth::Token.from_code(params[:code])

      if user = token.user
        session[:user_id] = user.id
      else
        # flash[:error] =  "Přihlášení KO"
      end

      # flash[:success] = "Přihlášení OK"
    else
      # TODO logovat důvod proč se nešlo přihlásit
      # flash[:error] = "Přihlášení KO"
    end

    redirect_to(session[:return_back] || root_url)

    session.delete :return_back
  end

  def logout
    session.delete(:user_id)

    # TODO redirect na click2stream
    redirect_to root_url
  end
end
