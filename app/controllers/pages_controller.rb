class PagesController < ApplicationController

  skip_before_filter :require_current_user

  def index
    if current_user and current_user.recordings.any?
      redirect_to recordings_url
    else
      @video = Video.where("image_fps < ?", 50)
                    .where.not(finished_at: nil)
                    .group(:schedule_id)
                    .order("RAND()")
                    .first

      render action: :make_new
    end
  end

end
