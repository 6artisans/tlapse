class Api::CamerasController < ApplicationController
  def detect_timezone
    @camera = current_user.camera(params[:id])

    respond_to do |f|
      f.json do
        if @camera.active_support_timezone
          render json: {timezone: {value: @camera.active_support_timezone.tzinfo.name, name: @camera.active_support_timezone.to_s}}
        else
          render json: {}
        end
      end
    end
  end
end