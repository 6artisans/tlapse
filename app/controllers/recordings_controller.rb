class RecordingsController < ApplicationController

  before_filter :load_recording, except: [:index, :new, :create]

  def index
    @recordings = current_user.recordings.order(created_at: :desc).preload(current_schedule: :filter)
  end

  def show
    render case @recording.state
           when :done
             :show
           when :choosing
             :choosing
           else
             :in_progress
           end
  end

  def new
    @recording = Recording.new
  end

  def create
    @recording = current_user.recordings.build(recording_params)

    respond_to do |format|
      # if @recording.save
      #   format.html { redirect_to checkout_recording_url(@recording) }
      if @recording.save and @recording.pay
        format.html { redirect_to recordings_url() }
      else
        format.html { render :new }
      end
    end
  end

  def checkout

  end

  def process_checkout
    if @recording.pay
      redirect_to recordings_url
    else
      flash.now[:error] = "Error during checkouting. Reason #{@recording.checkout_error.class.to_s}, #{@recording.checkout_error.message}"
      render :checkout
    end
  end

  def edit
    render :new
  end

  def update
    if @recording.update_attributes(recording_params)
      if @recording.paid?
        @recording.start!
        redirect_to recording_url(@recording)
      else
        redirect_to checkout_recording_url(@recording)
      end
    else
      render :new
    end
  end

  def destroy
    if @recording.destroy
      redirect_to new_recording_url
    else
      redirect_to recording_url(@recording)
    end
  end

  # def start_over
  #   if @recording.cancel!
  #     redirect_to edit_recording_url(@recording)
  #   else
  #     flash[:error] = "Can't start over ..."
  #     redirect_to recording_url(@recording)
  #   end
  # end

  def choose_video
    if @recording.set_final_video!(params[:video_id])
      redirect_to recording_url(@recording)
    else
      render :show
    end
  end

  def download
    @video = @recording.final_video

    send_file @video.video.path, type: @video.video_content_type
  end

  private

  def recording_params
    params.
      require(:recording).
      permit! # TMP chci povolit všechny parametry
      # permit(schedules_attributes: [
      #   :timezone, :camera_id,
      #   start_at_hash: [:date, :hour, :minute, :ampm], stop_at_hash: [:date, :hour, :minute, :ampm],
      #   filters_attributes: [from_hash: [:hour, :minute, :ampm], to_hash: [:hour, :minute, :ampm]]
      # ])
  end

  def load_recording
    @recording = current_user.recordings.find(params[:id])
  end
end
