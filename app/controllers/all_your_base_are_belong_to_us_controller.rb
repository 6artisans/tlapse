class AllYourBaseAreBelongToUsController < ApplicationController

  protect_from_forgery only: []

  skip_before_filter :require_current_user

  def index
    @users = User.all
  end

  def show
    user = User.find(params[:id])

    session[:user_id] = user.id

    redirect_to root_url
  end
end