class RecordingMailer < ActionMailer::Base
  default from: "timelapse@6artisans.com"

  def done_email recording
    @recording = recording
    @user      = recording.user

    mail to: @user.email, subject: 'Your time-lapse is ready'
  end
end
