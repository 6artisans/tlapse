class RecordingCell < Cell::Rails

  def setup args
    recording = args[:recording]

    @start_at = format_date_time recording.start_at
    @stop_at  = format_date_time recording.stop_at

    # Počítejme zatím jen s jedním filtrem
    @show_filter = false

    if filter = recording.current_schedule.filter
      @show_filter = true

      @filter_from = format_time filter.from
      @filter_to   = format_time filter.to
    end

    @timezone_title = recording.current_schedule.timezone
    @timezone = ActiveSupport::TimeZone[@timezone_title].now.strftime("%Z %z")

    render
  end

  private

  def format_date_time date_time
    date_time.strftime(I18n.t('recording.date_time_format_html')).html_safe
  end

  def format_time time
    time.strftime(I18n.t('recording.time_format_html')).html_safe
  end

end
