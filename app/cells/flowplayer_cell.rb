class FlowplayerCell < Cell::Rails

  include ::FontAwesome::Rails::IconHelper

  AVAILABLE_UI_OPTIONS = %w(volume mute length time)

  def show args
    @video_model = args[:video]
    @video       = @video_model.video
    @ratio       = @video_model.ratio.present? ? @video_model.ratio : 0.75

    @info        = args[:info]
    @choose      = args[:choose]
    @choose_icon = fa_icon("check", text: "Choose this video", right: true) if @choose
    @player      = args.fetch(:player) do {} end

    ui_options = AVAILABLE_UI_OPTIONS.dup

    if opts = args[:ui_options]
      opts.keys.each do |opt|
        ui_options.delete(opt.to_s)
      end
    end

    @ui_options = ui_options.map {|opt| "no-#{opt}" }.join(" ")

    render
  end

end
