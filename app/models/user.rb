# == Schema Information
#
# Table name: users
#
#  id          :integer          not null, primary key
#  remote_id   :integer
#  remote_data :text
#  token       :string(511)
#  created_at  :datetime
#  updated_at  :datetime
#

class User < ActiveRecord::Base

  serialize :token, TokenSerialization
  serialize :remote_data, OpenStructSerializer

  # Informace o userovi z C2S delegujeme do serializovaného fieldu
  delegate :firstname, :surname, :email, :company, :created_datetime,
           :address_street1, :address_street2, :address_city, :address_zip,
           to: :remote_data, allow_nil: true

  has_many :recordings, inverse_of: :user

  class << self
    # Stáhne si pro daný token info o uživateli z C2S a zkusí ho najít u nás v DB, pokud nenajde vytvoří nového
    # TODO handling chybových stavů?
    def find_or_create_by_token token
      if c2s_user = api(token).get("/me")
        c2s_user = OpenStruct.new c2s_user

        if id = c2s_user.id and user = User.where(remote_id: id).first
          user.token = token
          user.remote_data = c2s_user
          user.save
        else
          user = User.create(token: token, remote_id: id, remote_data: c2s_user)
        end

        return user
      end
    end

    def api token
      Click2Stream::Api.new token
    end
  end

  def token
    return @token if @token
    # Při načtení tokenu do něj vrazíme uživatele. Tím předejdeme jeho opakovanému načítání.
    if _token = super
      @token = Auth::Token.new(_token, :auth_code)
    end

    @token
  end

  def token= val
    @token = nil
    super
  end

  def api
    @api ||= self.class.api(token)
  end

  def camera id
    Camera.find(self, id)
  end

  def cameras
    @cameras ||= Camera.all_for_user(self)
  end

  def reload
    @cameras = nil
    @token = nil

    super
  end
end
