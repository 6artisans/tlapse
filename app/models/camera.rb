class Camera < ActiveModelLike

  def self.find user, id
    new user.api.get("/cameras/#{id}")
  end

  def self.all_for_user user
    cameras = user.api.get("/cameras")
    cameras.map {|camera_hash|
      new camera_hash
    }
  end

  def parsed_coords
    coords.split(",").map(&:strip).map(&:to_f)
  end

  def timezone
    return @timezone if defined?(@timezone)

    @timezone = if timezone_id
      Timezone::Zone.new zone: timezone_id
    elsif coords
      # TODO cachovat zónu. Máme omezený počet requestů
      Timezone::Zone.new latlon: parsed_coords
    end
  end

  def active_support_timezone
    @active_support_timezone ||= ActiveSupport::TimeZone[timezone.active_support_time_zone]
  end
end