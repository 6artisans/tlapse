# Převod snapshotů (obrázků) na video. Wrapper kolem ffmpeg.
class Transcoder
  class FFmpegError < StandardError
    attr_reader :transcoder

    def initialize(message = nil, transcoder)
      @transcoder = transcoder
      super("\n" + message)
    end
  end

  CODEC = "libx264"

  attr_reader :images_directory, :video_path, :image_fps, :video_fps

  def initialize images_directory, video_path, image_fps, video_fps
    @images_directory = images_directory
    @video_path       = video_path
    @image_fps        = image_fps
    @video_fps        = video_fps
  end

  # Na základě vložených parametrů vygeneruje ze snapshotů video.
  #
  # images_path  [string] - cesta ke složce s obrázky
  # video_path   [string] - cesta pro uložení výsledného videa
  # image_fps    [string] - frekvence se kterou jsou čteny obrázky na vstupu
  # video_fps    [string] - počet snímků / s ve výsledném videu
  def transcode!
    DirUtil.create_directory_if_not_exists(File.dirname(video_path))

    Rails.logger.info("Converting Images to Video with command:\n#{command}")

    output, error, status = Open3.capture3(command.gsub("\n", ""))

    if status.success?
      Rails.logger.info("Video successfully created. FFmpeg status: #{status}")
      true
    else
      raise FFmpegError.new(error, self)
    end
  end

private

  def command
<<-END
ffmpeg -r #{image_fps}
       -f image2
       -pattern_type glob
       -i '#{File.join(images_directory, "*.jpg")}'
       -vcodec #{CODEC}
       -pix_fmt yuv420p
       -r #{video_fps}
       -y
       -movflags faststart
       '#{video_path}'
END
  end
end
