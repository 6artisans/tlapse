class OpenStructSerializer
  class << self
    def dump something
      if something
        hash = something.to_h
        hash.to_json if hash.any?
      end
    end

    def load string
      OpenStruct.new(JSON.load(string))
    end
  end
end
