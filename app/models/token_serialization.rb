class TokenSerialization
  class << self
    def dump token
      token.to_json if token
    end

    def load serialized_val
      JSON.load(serialized_val)
    end
  end
end
