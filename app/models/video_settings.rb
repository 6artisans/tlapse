# == Schema Information
#
# Table name: video_settings
#
#  id         :integer          not null, primary key
#  max_frames :integer          not null
#  min_period :integer          not null
#  created_at :datetime
#  updated_at :datetime
#  video_fps  :integer          not null
#

# Nastaveni vytvoreneho videa
#
# Settings se nemazou, ale plati posledni vytvorene podle created_at.
# To se priradi kazdemu novemu recordingu a uz mu zustane. Tj. nove nastaveni
# plati jen pro nove recordingy, starym zustane puvodni.
#
class VideoSettings < ActiveRecord::Base
  scope :current, -> {order(created_at: :desc).first}
end
