# == Schema Information
#
# Table name: snapshotters
#
#  id            :integer          not null, primary key
#  start_at      :datetime
#  stop_at       :datetime
#  camera_id     :integer
#  period        :integer
#  remote_id     :integer
#  schedule_id   :integer
#  sent_at       :datetime
#  deleted_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  downloaded_at :datetime
#

# Obraz jobu ve snapshotter API
#
# To, co je tady ulozeno, je presne to, co se poslalo do API. Pokud snapshotter existuje,
# teoreticky by mel k nemu existovat job v API. Pokud ne, je to chyba. Pri smazani snap-
# shotteru se automaticky zavola odstraneni jobu.
#
# Testovaci provoz bez api: Apiable.skip_api_calls = true
#
class Snapshotter < ActiveRecord::Base

  acts_as_paranoid

  serialize :api_errors, JSON

  belongs_to :schedule

  delegate :images_path, to: :schedule

  before_destroy :destroy_in_api!, if: :remote_id?
  after_rollback :destroy_in_api!, on: :create, if: :remote_id?

  scope :not_in_api, -> {where(remote_id: nil)}

  def self.api
    @api ||= Click2Stream::SnapshotterApi.new(Auth::Token.client_token)
  end

  def api
    self.class.api
  end

  # TODO otestovat chybnou reakci API
  # vytvori svuj obraz ve snapshotovacim api
  def create_in_api!
    raise "Snapshotter already created in API with remote_id=#{remote_id}" if remote_id?

    response = api.create(start_datetime: start_at.utc,
                          stop_datetime:  stop_at.utc,
                          period:         period,
                          camera_id:      camera_id)

    update_attributes!(sent_at: Time.now.utc, remote_id: response["id"])
  end

  # smaze job odpovidajici sama sobe
  def destroy_in_api!
    transaction do
      api.destroy(self)
      update_attributes! remote_id: nil
    end
  end

  # vrati url obrazku, ktere udelalo snapshotovaci API
  def image_urls
    api.snapshots(self).map do |image, i|
      image["url"]
    end
  end

  # Vrátí url i s cestou kam ho uložit
  def images
    image_urls.map.with_index do |url, i|
      [url, file_path_for_image(i, url)]
    end
  end

  # stahne vsechny obrazky z API a ulozi je na danou lokalni cestu
  # pokud se podarilo stahnout vsechny, ulozi do downloaded_at aktualni cas
  # pri opakovanem spusteni stahne jen ty obrazky, ktere jeste v adresari nejsou
  def download_images!
    return true if downloaded_at
    result = perform_async_download_images!
    update_attributes!(downloaded_at: Time.zone.now)
  end

  # NEparalerní verze
  def perform_download_images!
    all_images = images

    images_to_download = filter_out_existing_images(all_images)

    DirUtil.create_directory_if_not_exists(images_path)

    images_to_download.each do |image|
      unless file_downloaded?(image)
        uri = URI(image)

        response = Net::HTTP.get_response(uri)

        File.open(file_path_for_image(image), 'wb') do |file|
          file.write(response.body)
        end
      end
    end

    raise "Not all images was downloaded correctly." if filter_out_existing_images(all_images).any?

    true
  end

  # Paralerní verze stahování obrázků
  def perform_async_download_images!
    all_images = images

    images_to_download = filter_out_existing_images(all_images)

    if images_to_download.any?
      DirUtil.create_directory_if_not_exists(images_path)

      wq = WorkQueue.new(Settings.images.concurrency_downloads)

      images_to_download.each do |image|
        wq.enqueue_b do
          uri = URI(image.first)
          response = Net::HTTP.get_response(uri)

          File.open(image.last, 'wb') do |file|
            file.write(response.body)
          end
        end
      end

      wq.join
    end

    raise "Not all images was downloaded correctly." if filter_out_existing_images(all_images).any?

    true
  end

  def recording_finished?
    (Time.now.utc + Settings.images.wait_before_download.seconds) >= stop_at.utc
  end

  # Trvání snapshotteru v sekundách
  def duration
    (stop_at - start_at).seconds
  end

  def frames
    (duration / period).floor
  end

  def duration_elapsed
    return 0.seconds if start_at >= Time.now.utc
    ([Time.now.utc, stop_at].min - start_at).seconds
  end

  def frames_elapsed
    (duration_elapsed / period).floor
  end

  def percent_elapsed
    duration_elapsed / duration * 100
  end
  ### Api Errors handling

  # Normalizace. Převedení string datetime na ruby Time
  def api_errors
    if result = super and result.is_a?(Array)
      result.map { |error| parse_api_error(error) }
    else
      []
    end
  end

  def api_errors?
    api_errors.any?
  end

  # Kolik času v sekundách byla kamera offline
  def duration_offline
    api_errors.map { |error| diff_error_datetime error.except(:message)}.sum.seconds
  end

  # Kolik snímků vypadlo z nahrávání
  def frames_offline
    (duration_offline / period).floor
  end

  # Kolik procent z celkového nahrávání nebylo zaznamenáno
  def percent_offline
    return 0 if duration_elapsed == 0
    duration_offline / duration_elapsed * 100
  end

  def update_api_errors!
    self.api_errors = self.api.errors(self)
    self.save
  end

  private

  def parse_api_error error
    {
      message:       error["message"],
      from_datetime: error["from_datetime"] ? Time.parse(error["from_datetime"]) : nil,
      to_datetime:   error["to_datetime"] ? Time.parse(error["to_datetime"]) : nil
    }
  end

  def diff_error_datetime from_datetime:, to_datetime: nil
    to_datetime ||= [Time.now.utc, stop_at].min
    to_datetime - from_datetime
  end

  # Vyfiltruje již stažené obrázky a vrátí jen ty, které je potřeba stáhnout
  def filter_out_existing_images images
    images.reject { |image| file_downloaded?(image.last) }
  end

  def file_downloaded? file
    File.exist?(file) && File.size(file) > 0
  end

  # Cesta k souboru na disku
  def file_path_for_image i, url
    File.join images_path, "#{self.start_at.to_i}-#{i.to_s.rjust(6,"0")}-#{normalize_url(url)}"
  end

  # Odstraní nepotřebné části url obrázku
  # https://s3-us-west-2.amazonaws.com/snapshotting-test.angelcam.com/2/6390/20141006/135646snlbj74hfa.jpg
  # převede na
  # 2-6390-20141006-135646snlbj74hfa.jpg
  def normalize_url url
    path_parts = URI(url).path.split("/")

    while not path_parts.first =~ /\d+/
      path_parts.shift
    end

    path_parts.join("-")
  end
end

