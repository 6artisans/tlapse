# == Schema Information
#
# Table name: videos
#
#  id                 :integer          not null, primary key
#  schedule_id        :integer
#  duration           :integer          not null
#  image_fps          :float(24)        not null
#  video_fps          :float(24)        not null
#  created_at         :datetime
#  updated_at         :datetime
#  finished_at        :datetime
#  video_file_name    :string(255)
#  video_content_type :string(255)
#  video_file_size    :integer
#  video_updated_at   :datetime
#  deleted_at         :datetime
#

class Video < ActiveRecord::Base

  acts_as_paranoid

  has_attached_file :video, preserve_files: true,
                            url:            "/system/:attachment/:id_partition/:hash.:extension",
                            hash_secret:    "my_little_secret_which_you_dont_know"

  validates_attachment_content_type :video, content_type: ['video/mp4']

  VIDEO_FORMAT = "mp4"

  belongs_to :schedule

  before_validation :set_rates

  delegate :video_settings, :video_height, :video_width, to: :schedule

  def name
    "#{schedule.recording.name.parameterize}-#{duration}"
  end

  def extension
    ".#{VIDEO_FORMAT}"
  end

  def file_name
    name + extension
  end

  def generate_video!
    # Vytvoříme tempfile se správnou příponout. Ta je potřeba pro FFmpeg. Ten jinak stávkuje a odmítá vygenerovat video pokud přípona neodpovídá
    temp = Tempfile.new([digest_name, extension])

    # Do něj vygenerujeme video
    transcoder = Transcoder.new(schedule.images_path, temp.path, image_fps, video_fps)
    transcoder.transcode!

    # Video uložíme do modelu
    self.video = File.open(temp)
    # Malý trik, kdy přepíšeme náhodné jméno tempfilu na název souboru jaký chceme
    self.video.instance_write(:file_name, file_name)

    save!
  ensure
    # Tempfile vždy zavřeme a smažeme
    temp.close
    temp.unlink
  end

  def update_ratio!
    if video_height and video_width
      self.ratio = video_height / video_width.to_f
      self.save validate: false
    end
  end

  private

  def set_rates
    self.video_fps = video_settings.video_fps
    self.image_fps = (schedule.real_frames_count / duration.to_f).round(2)
  end

  def digest_name
    Digest::MD5.hexdigest("#{name}.#{video_fps}.#{image_fps}.#{created_at}")
  end
end
