# == Schema Information
#
# Table name: recordings
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  attempts             :integer          default(0), not null
#  video_settings_id    :integer
#  created_at           :datetime
#  updated_at           :datetime
#  final_video_id       :integer
#  user_id              :integer          not null
#  deleted_at           :datetime
#  preview_file_name    :string(255)
#  preview_content_type :string(255)
#  preview_file_size    :integer
#  preview_updated_at   :datetime
#

# Jedna koupena sluzba timelapse, s nazvem a informaci o zaplaceni
class Recording < ActiveRecord::Base

  attr_accessor :checkout_error

  # TODO extrahovat rozlišení obrázku https://github.com/thoughtbot/paperclip/wiki/Extracting-image-dimensions
  has_attached_file :preview, styles:         {thumb: "300x150>"},
                              preserve_files: true,
                              url:            "/system/:attachment/:id_partition/:hash.:extension",
                              hash_secret:    "my_little_secret_which_you_dont_know"#, # TODO přesunout do configu
                              # default_url:    "/images/:style/missing.png" # TODO něco jiného :)

  validates_attachment_content_type :preview, :content_type => //i # TODO validace stazeneho obrazku moc nefunguje

  before_save :extract_video_dimensions_from_preview

  acts_as_paranoid

  has_many :schedules, inverse_of: :recording, dependent: :destroy

  has_one :current_schedule, -> () { where(canceled_at: nil).order(created_at: :desc) },
          class_name: :Schedule,
          inverse_of: :recording

  accepts_nested_attributes_for :current_schedule
  validates :current_schedule, associated: true

  belongs_to :final_video, class_name: "Video"
  belongs_to :video_settings
  belongs_to :user

  before_validation :assign_camera_name
  before_create :assign_video_settings!

  delegate :start_at, :stop_at, to: :current_schedule
  delegate :api, to: :user

  def cancel!
    current_schedule.destroy
  end

  def set_final_video! id
    if video = current_schedule.videos.find(id)
      update_attribute(:final_video, video)
    end
  end

  def start!
    # Pokud selže vytváření Jobu, nestrhneme klientovi pokus
    Recording.transaction do
      increase_attempts!
      Daemons::Planner::create_snapshotters!(current_schedule)
    end
  end

  # Kód stavu v kterém se nahrávka nachází
  # before_start
  # recording
  # generating
  # choosing
  # done
  def state
    if current_schedule.error?
      :error
    elsif final_video_id?
      :done
    elsif current_schedule.video_generated?
      :choosing
    elsif Time.now.utc < current_schedule.make_utc_time(current_schedule.start_at)
      :before_start
    elsif Time.now.utc < current_schedule.make_utc_time(current_schedule.stop_at)
      :recording
    else
      :generating
    end
  end

  # TODO jen pro debug účely než bude vymyšlené placení ze strany C2S
  def paid?
    true
  end

  def pay
    start!
  rescue => ex
    self.checkout_error = ex
    false
  end

  def pay!
    start!
  end

  def update_video_dimensions!
    save(validate: false) if extract_video_dimensions_from_preview
  end

  private

  def assign_camera_name
    self.name = current_schedule.camera.shortname if current_schedule.camera_id_changed?
  end

  def increase_attempts!
    increment! :attempts
  end

  def assign_video_settings!
    self.video_settings = VideoSettings.current
  end

  def extract_video_dimensions_from_preview
    return unless preview?

    if pending_preview = preview.queued_for_write[:original]
      path = pending_preview.path
    else
      path = preview.staged_path || preview.path
    end

    return if path.nil?

    geometry = Paperclip::Geometry.from_file(path)

    self.video_height = geometry.height.to_i
    self.video_width  = geometry.width.to_i

    true
  end
end
