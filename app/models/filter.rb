# == Schema Information
#
# Table name: filters
#
#  id          :integer          not null, primary key
#  from        :time
#  to          :time
#  schedule_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#  deleted_at  :datetime
#

class Filter < ActiveRecord::Base

  self.skip_time_zone_conversion_for_attributes = [:from, :to]

  acts_as_paranoid

  belongs_to :schedule

  validates :from, :to, presence: {message: "must be in a correct format"}

  attr_reader :from_hash, :to_hash

  [:from, :to].each do |mthd|
    define_method :"#{mthd}_hash=" do |hsh|
      if hsh[:hour].present? && hsh[:minute].present? && hsh[:ampm].present?
        time = Time.parse("2000/1/1 #{hsh[:hour]}:#{hsh[:minute]} #{hsh[:ampm]}")
        send :"#{mthd}=", time.strftime("%H:%M")
      end

      instance_variable_set(:"@#{mthd}_hash", hsh)
    end
  end
end
