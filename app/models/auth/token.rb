# TODO rozpadnout na ::Client a ::ClientCredentials
class Auth::Token
  class << self
    # Token typu `client_credentials`
    def client_token
      @client_token ||= new(Auth.client.client_credentials.get_token, :client_credentials)
    end

    # Vytvoření tokenu z `code`, který nám vrací c2s při loginu uživatele
    def from_code code
      new(Auth.client.auth_code.get_token(code), :auth_code)
    end
  end

  attr_reader :token
  attr_writer :user

  delegate :to_hash, :to_json, to: :token

  # Inicializace našeho Token wrapu
  def initialize token, type
    @token = build(token)
    @type  = type
  end

  # Zkontroluje, zda token nevypršel a pokud ano, obnovího a nový token uloží do db
  def check_expiry!
    if @token.expired?
      t1 = Time.now.to_f

      if @type == :auth_code
        Rails.logger.info("OAuth2::AccessToken refreshing auth_code token")

        @token = @token.refresh!
      else
        Rails.logger.info("OAuth2::AccessToken getting new client_credentials token")

        @token = Auth.client.client_credentials.get_token
      end

      Rails.logger.info("Token refreshed in #{(Time.now.to_f - t1).round(3)} seconds")

      user.update_attribute(:token, self) if user
    end

    self
  end

  # Obalení funkcí pro http requesty aby kontrolovaly platnost tokenu
  [:get, :post, :put, :delete, :patch, :head, :options].each do |type|
    define_method type do |*args|
      check_expiry!
      @token.send type, *args
    end
  end

  # Zkusí najít uživatele pro daný token, pokud ho nenajde pokusí se ho vytvořit a nahrát mu údaje
  # TODO odchytávat, popř vyhazovat nějaké chyby
  def user
    @user ||= User.find_or_create_by_token(self)
  end

  private

  # Vytvoří instanci OAuth2::AccessToken z hashe, nebo vrátí sebe samu
  def build token
    case token
    when OAuth2::AccessToken
      token
    when Hash
      OAuth2::AccessToken.from_hash(AuthClient, token)
    else
      raise "Cant initialize `OAuth2::AccessToken` from klass `#{token.class}` content `#{token.inspect}`"
    end
  end
end
