class Daemons::PartialImageDownloader
  # extend Daemons::Cancelable
  include Sidekiq::Worker
  sidekiq_options queue: "image_downloader", retry: 5

  sidekiq_retry_in do |count|
    30
  end

  def perform snapshotter_id
    snapshotter = Snapshotter.with_deleted.find(snapshotter_id)

    unless snapshotter.destroyed?
      Sidekiq.logger.info("Downloading partial images for snapshotter: #{snapshotter.inspect}")

      snapshotter.perform_async_download_images!
    else
      Sidekiq.logger.info("Ignored, because snapshotter `#{snapshotter.id}` was destroyed.")
    end
  end
end
