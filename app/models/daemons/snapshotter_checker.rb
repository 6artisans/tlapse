class Daemons::SnapshotterChecker
  # extend Daemons::Cancelable
  include Sidekiq::Worker
  sidekiq_options queue: "snapshotter_checker"

  def perform snapshotter_id
    snapshotter = Snapshotter.with_deleted.find(snapshotter_id)

    unless snapshotter.destroyed?
      Sidekiq.logger.info("Checking snapshotter id=#{snapshotter.id} job id=#{snapshotter.remote_id}")

      # Zaktualizujeme data o chybách
      snapshotter.update_api_errors!

      # Načteme schedule
      schedule = snapshotter.schedule

      # Pokud nemůžeme vygenerovat video, nastavíme chybovou hlášku
      unless schedule.can_generate_video?
        schedule.error! I18n.t('schedule.camera_is_too_much_offline')
      else
        schedule.scheduled!
      end

      # Stáhneme obrázek
      if images = snapshotter.image_urls and image = images.last
        Daemons::Planner.create_preview! snapshotter, image
      end

      # Spočítáme další interval pro checknutí
      check_interval    = [snapshotter.duration / 20, 10.minutes].max
      next_check_at     = Time.now.utc + check_interval
      waiting           = Settings.images.wait_before_download.seconds
      image_download_at = snapshotter.stop_at + waiting

      if next_check_at >= image_download_at
        next_check_at = image_download_at
      end

      # Pokud už nahrávání doběhlo a můžeme vygenrovat video, tak nastavíme stahování obrázků
      if snapshotter.recording_finished?
        Daemons::Planner.download_images! snapshotter
      # Jinak nastavíme další checker a stáhneme temp obrázky
      else
        Daemons::Planner.download_partial_images! snapshotter
        Daemons::Planner.check_snapshotter_at! next_check_at, snapshotter
      end
    else
      Sidekiq.logger.info("Ignore, because snapshotter `#{snapshotter.id}` was destroyed.")
    end
  end
end
