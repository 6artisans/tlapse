class Daemons::RecordingDone
  # extend Daemons::Cancelable
  include Sidekiq::Worker
  sidekiq_options queue: "recording_done", retry: true

  def perform recording
    recording = Recording.with_deleted.find(recording)

    unless recording.destroyed?
      RecordingMailer.done_email(recording).deliver
    else
      Sidekiq.logger.info("Ignored, because recording `#{recording.id}` was destroyed.")
    end
  end
end
