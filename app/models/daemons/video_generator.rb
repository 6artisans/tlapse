class Daemons::VideoGenerator
  # extend Daemons::Cancelable
  include Sidekiq::Worker
  sidekiq_options queue: "video_generator"

  # bezi po uspesnem stazeni vsech snapshotteru
  def perform video_id
    video = Video.with_deleted.find(video_id)

    unless video.destroyed?
      Sidekiq.logger.info("Generating video with id `#{video.id}`")

      video.generate_video!
      video.update_attribute :finished_at, Time.zone.now

      schedule = video.schedule

      if schedule.videos_generated?
        schedule.video_generated!
        schedule.remove_downloaded_images!
        Daemons::Planner.send_done_email schedule.recording
      end
    else
      Sidekiq.logger.info("Ignoring, because video `#{video.id}` was destroyed.")
    end
  end
end