class Daemons::PreviewDownloader
  # extend Daemons::Cancelable
  include Sidekiq::Worker
  sidekiq_options queue: "preview_downloader", retry: false

  def perform snapshotter_id, image_url
    snapshotter = Snapshotter.with_deleted.find(snapshotter_id)

    unless snapshotter.destroyed?
      Sidekiq.logger.info("Downloading preview snapshotter: #{snapshotter.id}")

      snapshotter.schedule.recording.tap do |r|
        r.preview = URI(image_url)
        r.save validate: false
      end
    else
      Sidekiq.logger.info("Ignored, because snapshotter `#{snapshotter.id}` was destroyed.")
    end
  end

  def prepare_videos! schedule
    schedule.prepare_videos!
  end

  def schedule_videos! schedule
    schedule.videos.each do |video|
      Daemons::Planner.generate_video!(video)
    end
  end
end
