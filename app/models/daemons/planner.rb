# Planovac asynchronnich tasku
class Daemons::Planner
  class << self
    def create_snapshotters! schedule
      Rails.logger.info("Creating job: Daemons::SnapshotterCreator(#{schedule.id})")

      Daemons::SnapshotterCreator.perform_sync schedule.id
      # Daemons::SnapshotterCreator.perform_async schedule.id
    end

    def create_preview! snapshotter, url
      Rails.logger.info("Scheduling job: Daemons::PreviewDownloader(#{snapshotter.id})")
      Daemons::PreviewDownloader.perform_async(snapshotter.id, url)
    end

    def check_snapshotter_at! utc_datetime, snapshotter
      Rails.logger.info("Scheduling job: Daemons::SnapshotterChecker(#{snapshotter.id}) at #{utc_datetime.to_s(:db)}")
      Daemons::SnapshotterChecker.perform_at(utc_datetime, snapshotter.id)
    end

    def download_partial_images! snapshotter
      Rails.logger.info("Scheduling job: Daemons::PartialImageDownloader(#{snapshotter.id})")
      Daemons::PartialImageDownloader.perform_async(snapshotter.id)
    end

    def download_images! snapshotter
      Rails.logger.info("Scheduling job: Daemons::ImageDownloader(#{snapshotter.id})")
      Daemons::ImageDownloader.perform_async(snapshotter.id)
    end

    def download_images_at! utc_datetime, snapshotter
      Rails.logger.info("Scheduling job: Daemons::ImageDownloader(#{snapshotter.id}) at #{utc_datetime.to_s(:db)}")
      Daemons::ImageDownloader.perform_at(utc_datetime, snapshotter.id)
    end

    def generate_video! video
      Rails.logger.info("Creating job: Daemons::VideoGenerator(#{video.id})")
      Daemons::VideoGenerator.perform_async(video.id)
    end

    def send_done_email recording
      Rails.logger.info("Creating job: Daemons::RecordingDone(#{recording.id})")
      Daemons::RecordingDone.perform_async(recording.id)
    end
    # Pouze pro debug účely
    def create_schedule! recording, params
      schedule = recording.schedules.create(params)
      create_snapshotters!(schedule) unless schedule.errors.any?
      schedule
    end
  end
end