class Daemons::ImageDownloader
  # extend Daemons::Cancelable
  include Sidekiq::Worker
  sidekiq_options queue: "image_downloader"

  sidekiq_retry_in do |count|
    15 + (count * 2)
  end

  def perform snapshotter_id
    snapshotter = Snapshotter.with_deleted.find(snapshotter_id)

    unless snapshotter.destroyed?
      Sidekiq.logger.info("Downloading images for snapshotter: #{snapshotter.inspect}")

      if snapshotter.download_images!
        Sidekiq.logger.info("Images successfully downloaded for snapshotter: #{snapshotter.id}")

        schedule = snapshotter.schedule

        # Pokud jsou již obrázky staženy
        if schedule.images_ready?

          # A můžeme vygenerovat video (malá chybovost)
          if schedule.can_generate_video?
            schedule.change_state!(:images_downloaded)

            prepare_videos!  schedule
            schedule_videos! schedule

          # Jinak smažeme stažené obrázky
          else
            schedule.remove_downloaded_images!
          end
        end
      end
    else
      Sidekiq.logger.info("Ignored, because snapshotter `#{snapshotter.id}` was destroyed.")
    end
  end

  def prepare_videos! schedule
    schedule.prepare_videos!
  end

  def schedule_videos! schedule
    schedule.videos.each do |video|
      Daemons::Planner.generate_video!(video)
    end
  end
end
