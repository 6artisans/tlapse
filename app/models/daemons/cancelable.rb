module Daemons::Cancelable
  def cancel *args
    Sidekiq::ScheduledSet.new.each do |job|
      if self.to_s == job.klass and job.args == args
        job.delete
        Rails.logger.info("Canceling job: #{self.to_s}(#{args.join(", ")})")
      end
    end
  end
end