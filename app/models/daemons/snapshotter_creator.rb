class Daemons::SnapshotterCreator
  # extend Daemons::Cancelable
  include Sidekiq::Worker
  sidekiq_options queue: "snapshotter_creator"

  def self.perform_sync schedule_id
    new.perform(schedule_id)
  end

  def perform schedule_id
    schedule = Schedule.with_deleted.find(schedule_id)

    unless schedule.destroyed?
      Sidekiq.logger.info("Creating snappshotters for schedule: #{schedule.inspect}")

      # TODO pokud by se stalo, že uživatel vytvoří timelapse, který začíná např za minutu a než to probublá ve frontě k akci, může být již start_time < Time.now a pak se timelapse vůbec nezprocesuje. V takovém případě ignorovat, že je již "pozdě," nebo mu dát možnost vytvořit nový timelapse. Stený problém nastane pokud vytváření Screenshotterů spadne a do fronty se zařadí s minutovým timeoutem.
      schedule.create_snapshotters!

      send_snapshotters_to_api! schedule
      schedule_checkers!        schedule

      schedule.change_state! :scheduled
    else
      Sidekiq.logger.info("Ignore, because schedule `#{schedule.id}` was destroyed.")
    end
  end

  # posle vsechny snapshottery do API, vrati true, pokud se vsechny vytvorily
  def send_snapshotters_to_api! schedule
    schedule.snapshotters.not_in_api.all? do |snapshotter|
      snapshotter.create_in_api!
    end
  end

  def schedule_checkers! schedule
    schedule.snapshotters.each do |snapshotter|
      Daemons::Planner.check_snapshotter_at!(snapshotter.start_at + Settings.images.wait_before_download.seconds, snapshotter)
    end
  end
end
