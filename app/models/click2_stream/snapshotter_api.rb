class Click2Stream::SnapshotterApi < Click2Stream::Api

  self.server = Settings.click2stream_api.snapshotter_url

  def show ss, params = {}
    get! "/jobs/#{ss.remote_id}", params
  end

  def create params
    post! "/jobs", params
  end

  def update ss, params
    # TODO. zatím ale nepotřebujeme
  end

  def destroy ss
    delete! "/jobs/#{ss.remote_id}"
  end

  def snapshots ss
    get! "/jobs/#{ss.remote_id}/snapshots"
  end

  def errors ss
    get! "/jobs/#{ss.remote_id}/errors"
  end
end
