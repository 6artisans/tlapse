class Click2Stream::Api
  class CallError < StandardError; end

  class << self

    attr_accessor :server, :skip_api_calls

    def server?
      !!@server
    end

    # query the server
    # token  = instance of Auth::Token
    # method = get|post|put|delete
    # url    = url, or path
    # data   = params - for get request send as url params, for others as serialized json body
    # params = can be headers, body etc
    def query tkn, method, url, data = {}, params = {}
      self.query!(tkn, method, url, data, params)
    rescue
      nil
    end

    def query! tkn, method, url, data = {}, params = {}
      # Some default headers
      headers = {
        content_type: "application/json",
        accept: "application/json",
        accept_encoding: "gzip, deflate"
      }

      headers = headers.merge(params[:headers]) if params[:headers]

      call_params = {headers: headers}

      case method.to_sym
      when :get, :head, :delete
        call_params[:params] = data
      else
        call_params[:body] = data.to_json
      end

      if self.skip_api_calls
        {}
      else
        Rails.logger.info("#{name} AT::#{Time.now.strftime("%Y-%m-%d %H:%M:%S %z")} VIA::#{method.upcase} ON::#{url}, WITH::#{data}")
        t1 = Time.now.to_f

        response = tkn.send(method.to_sym, url, call_params)

        Rails.logger.info("#{name} successfully responded in #{(Time.now.to_f - t1).round(3)} seconds")

        if response and parsed = response.parsed
          Rails.logger.debug("#{name} response was:#{parsed.to_s.truncate(512)}")
          return parsed
        else
          Rails.logger.debug("#{name} response was EMPTY")
          return
        end
      end
    rescue => ex
      raise CallError.new("During request FOR::#{name} VIA::#{method.upcase} ON::#{url}, WITH::#{data}, ERROR::#{ex.class}, REASON::#{ex.message}")
    end
  end

  def initialize tkn
    @token = tkn
  end

  # TODO: použít URI ruby entitu
  def make_url(path)
    self.class.server? ? File.join(val(self.class.server), val(path)) : val(path)
  end

  # all arguments can be lambdas, so evals the arguments
  def val(param)
    case param
    when Proc
      param.call
    when Symbol
      send(param)
    else
      param
    end
  end

  [:get, :post, :put, :delete, :patch, :head, :options].each do |method|
    define_method("#{method}!") do |url, data = {}, params = {}|
      self.class.query!(@token, method, make_url(url), val(data), params)
    end

    define_method(method) do |url, data = {}, params = {}|
      self.class.query(@token, method, make_url(url), val(data), params)
    end
  end
end
