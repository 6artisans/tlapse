# == Schema Information
#
# Table name: schedules
#
#  id            :integer          not null, primary key
#  start_at      :datetime
#  stop_at       :datetime
#  timezone      :string(255)
#  camera_id     :integer
#  recording_id  :integer
#  scheduled_at  :datetime
#  finished_at   :datetime
#  canceled_at   :datetime
#  filters       :text
#  created_at    :datetime
#  updated_at    :datetime
#  frames        :integer
#  state         :integer          default(0)
#  deleted_at    :datetime
#  error_message :string(255)
#

# Nastaveni nahravani, tj. od-do, timezone, kamera
#
# Krome standardnich created_at a updated_at ma trida dalsi timestampy:
#   scheduled_at - kdy byl schedule zpracovan a uspesne vytvoreny screenshottery
#   finished_at  - dokud toto neni nastaveno, neni video k dispozici
#   canceled_at  - kdy byl schedule zrusen uzivatelem (vycerpany attempt daneho recordingu)
#
# Timestampy jsou ulozene v UTC, udaje zadane uzivatelem jsou v casove zone uzivatele, ktera
# je ulozena v atributu timezone. Tj.:
#     start_at: "2014-04-04 20:13:00", stop_at: "2014-04-07 20:13:00",
#     filters: [{from: "08:00", to: "12:00"}], timezone: "Europe/Prague", camera_id: 1)
#
# Testovaci provoz bez api: Apiable.skip_api_calls = true
#
class Schedule < ActiveRecord::Base

  acts_as_paranoid

  belongs_to :recording
  has_many :snapshotters, dependent: :destroy

  has_many :videos, dependent: :destroy
  # Umíme pracovat s více filtry
  has_many :filters, dependent: :destroy
  # Ale na frontendu uživateli prezentujeme jen jeden
  has_one  :filter, dependent: :destroy

  accepts_nested_attributes_for :filter, reject_if: :filter_attributes_empty?

  validates_associated :filter

  self.skip_time_zone_conversion_for_attributes = [:start_at, :stop_at]

  validates :start_at, :stop_at, presence: {message: "must be in a correct datetime format"}
  validates :timezone, presence: true
  validate :validates_dates, if: -> {[nil, Schedule.states["created"]].index(state)}

  before_create :calculate_frames!
  before_destroy :remove_downloaded_images!

  delegate :api, :video_settings, :video_height, :video_width, to: :recording

  # Stav plánovače
  enum state: {
    created:            0,
    scheduled:          5,
    images_downloaded: 10,
    video_generated:   20,
    canceled:          30,
    error:             90
  }

  attr_reader :start_at_hash, :stop_at_hash

  [:start_at, :stop_at].each do |mthd|
    define_method :"#{mthd}_hash=" do |hsh|
      send :"#{mthd}=", "#{hsh[:date]} #{hsh[:hour]}:#{hsh[:minute]} #{hsh[:ampm]}"
      instance_variable_set(:"@#{mthd}_hash", hsh)
    end
  end

  def camera
    @camera ||= recording.user.camera(camera_id)
  end

  # Byla už vygenerována všechna videa?
  def videos_generated?
    return false if videos.blank?
    videos.all?(&:finished_at?)
  end

  def images_ready?
    snapshotters.all?(&:downloaded_at)
  end

  # vytvori snapshotter joby podle nastavenych limitu
  def create_snapshotters!
    calculate_frames!
    period = duration / frames

    to_intervals.each do |from, to|
      create_snapshotter!(from, to, period)
    end

    update_attribute :scheduled_at, Time.zone.now
  end

  # rozdeli na intervaly podle jednotlivych snapshotovacich jobu v utc
  def to_intervals
    # kdyz nejsou definovane filtry, je to jednoduche
    return [[make_utc_time(start_at), make_utc_time(stop_at)]] unless filter

    # s filtry je to slozitejsi, bereme den po dni
    day_count = days_diff(start_at, stop_at)

    utc_start_at = make_utc_time(start_at)
    utc_stop_at = make_utc_time(stop_at)

    day_count.times.reduce([]) do |intervals, day_offset|
      is_first_day = day_offset == 0

      offset_start_date = (start_at + day_offset.day).to_date

      [filter].map do |filter|
        last_interval = intervals.last
        next_interval = []

        # Začátek intervalu
        date_time_from = is_first_day ? [utc_start_at, make_utc_time(start_at, filter.from)].max
                                      : make_utc_time(offset_start_date, filter.from)

        if date_time_from >= utc_stop_at or (last_interval and last_interval.last >= date_time_from)
          next intervals
        else
          next_interval << date_time_from
        end

        # Je filtr přez půlnoc?
        filter_reversed = filter.from > filter.to

        offset_end_date = filter_reversed ? offset_start_date + 1.day : offset_start_date

        date_time_to    = [utc_stop_at, make_utc_time(offset_end_date, filter.to)].min

        next_interval << date_time_to

        intervals << next_interval
      end

      intervals
    end
  end

  # vypocita pocet obrazku, ktere se maji udelat, ulozi do atributu frames
  def calculate_frames!
    max_frames = duration / video_settings.min_period
    self.frames = [max_frames, video_settings.max_frames].min
  end

  # kolik bylo skutečně pořízeno obrázků
  def real_frames_count
    Dir[File.join(images_path, '**', '*')].length
  end

  # celkovy sledovany cas ve vterinach
  def duration
    to_intervals.map { |a,b| time_diff(a, b) }.sum.seconds
  end

  # cesta k obrazkum, ze kterych se udela video
  def images_path
    File.join(Rails.root, Settings.images.directory, id.to_s)
  end

  # zkonvertuje datetime dle timezone v instanci
  # pokud je zadany time, pouzije time misto casu v datetime
  # TODO udělat z toho helper funkci někde, protože se to volá z více míst a je to závislé jen na timezone v tomto modelu
  # def make_utc_time(datetime, timezone, overridden_time = nil)
  def make_utc_time(datetime, overridden_time = nil)

    # Pokud je na vstupu Date, převedeme ho na utc čas
    if datetime.is_a?(Date)
      datetime = datetime.to_time(:utc)
    end

    # Přepsání hodin, minut a sekund z Filter
    if overridden_time.present?
      hour, minute, second = _convert_to_time(overridden_time).split(":")
      datetime = datetime.change hour: hour, min: minute, sec: second
    end

    # Převod timezone
    if timezone?
      # TODO možná by šlo použít rovnou `local_to_utc` na `active_support_timezone`
      tzinfo = active_support_timezone.tzinfo

      # Pomocí ní převedeme uživatelův čas, který je v té timezone na UTC
      datetime = tzinfo.local_to_utc(datetime)
    end

    datetime
  end

  def active_support_timezone
    return unless timezone?
    tz = Timezone::Zone.new zone: timezone

    ActiveSupport::TimeZone[tz.active_support_time_zone]
  end

  def recording?
    make_utc_time(stop_at) > Time.now
  end

  def recording_time_left
    make_utc_time(stop_at) - Time.now
  end

  def change_state! new_state
    update_attribute :state, Schedule.states[new_state.to_s]
  end

  def prepare_videos!
    Settings.videos_durations.each do |duration|
      self.videos.
           where(duration: duration).
           create_with(ratio: (video_height and (video_height / video_width.to_f))).
           first_or_create!
    end
  end


  def video_generated!
    super
    # TODO poslat notifikaci uživateli, že jsou videa hotová

    # Smažeme stažené obrázky, které nepotřebujeme již. Dají se stáhnout znova kdyžtak
    remove_downloaded_images!
  end

  def remove_downloaded_images!
    DirUtil.remove_directory_if_exists(images_path)
  end

  def recording_finished?
    make_utc_time(stop_at) >= Time.now.utc
  end

  # Errors handling
  def error! message
    super()
    update_attribute :error_message, message
  end

  def percent_offline
    ss = snapshotters.to_a

    ss.any? ? ss.map(&:percent_offline).sum / ss.length : 0
  end

  def percent_elapsed
    ss = snapshotters.to_a

    ss.any? ? ss.map(&:percent_elapsed).sum / ss.length : 0
  end

  def can_generate_video?
    return true if percent_elapsed < 33
    percent_offline <= 50
  end

  private

  def validates_dates
    errors.add(:stop_at, "must be after start time") if start_at && stop_at && stop_at < start_at
    [:start_at, :stop_at].each { |col| validate_date_in_future(col) }
  end

  def validate_date_in_future col
    date_time = send(col)

    if date_time && make_utc_time(date_time) <= Time.now.utc
      errors.add(col, "must be in the future")
    end
  end

  # vytvori snapshotter od-do s ostatnimi parametry instance
  def create_snapshotter! start, stop, period
    snapshotters.where(
      start_at:  start,
      stop_at:   stop,
      camera_id: camera_id,
      period:    period
    ).first_or_create!
  end

  def time_diff t1, t2
    (t2 - t1).floor
  end

  def days_diff t1, t2
    (t2.to_date - t1.to_date).ceil + 1
  end

  def max_time(t1, t2)
    convert_to_time(t1, t2).max
  end

  def min_time(t1, t2)
    convert_to_time(t1, t2).min
  end

  def convert_to_time(*ts)
    ts.map {|t| _convert_to_time(t) }
  end

  def _convert_to_time(t)
    t.kind_of?(String) ? t : t.strftime("%T")
  end

  def filter_attributes_empty? attrs
    attrs["from_hash"].except("ampm").all? {|_,v| v.empty? } && attrs["to_hash"].except("ampm").all? {|_,v| v.empty? }
  end
end
