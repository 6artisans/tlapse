class ActiveModelLike
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  def initialize(attributes = {})
    attributes.each { |name, value|
      create_attr(name, value)
    }
  end

  def persisted?
    false
  end

  def create_attr(name, value)
    self.class.class_eval { attr_accessor name.to_sym }

    if(value.instance_of?(Array))
      send "#{name}=", value.collect { |v| class_for(name).new(v) }
    else
      send "#{name}=", value
    end
  end

  private

  def method_missing(method_name, *args, &block)
    match = method_name.to_s.match(/(.*?)([?]?)$/)
    case match[2]
    when "?"
      instance_variable_defined?("@#{match[1]}")
    end
  end

  def class_for(key)
    Object.const_get(key.to_s.classify)
  end
end