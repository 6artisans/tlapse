class DirUtil
  class << self
    def create_directory_if_not_exists pathname
      FileUtils.mkdir_p(pathname) unless Dir.exist?(pathname)
    end

    def remove_directory_if_exists pathname
      FileUtils.rm_r(pathname) if Dir.exist?(pathname)
    end
  end
end
