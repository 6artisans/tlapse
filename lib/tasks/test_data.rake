namespace :app do
  desc <<-DESC
    Load testing data.
    Run using the command 'rake app:load_demo_data'
  DESC

  task :load_demo_data => [:environment] do
  # Only data not required in production should be here.
  # If it needs to be there in production, it belongs in seeds.rb.

  Recording.delete_all
  Schedule.delete_all
  Snapshotter.delete_all
  Video.delete_all
  VideoSettings.delete_all

  vs = VideoSettings.create!(max_frames: 1440, min_period: 10)

  r1 = Recording.create!(name: "Bathroom camera", attempts: 1, video_settings: vs)

  s1 = Schedule.new(start_at: Time.now - 2.days,
                    stop_at: Time.now + 1.week,
                    timezone: "Europe/Prague",
                    camera_id: 1,
                    recording: r1,
                    filters: [],
                    canceled_at: Time.now - 1.day)
  s1.save!(validate: false)

  s2 = Schedule.new(start_at: Time.now - 1.day,
                    stop_at: Time.now + 1.week,
                    timezone: "Europe/Prague",
                    camera_id: 1,
                    recording: r1,
                    filters: [{ from: "06:00", to: "12:00" },
                              { from: "13:00", to: "19:30" }])
  s2.save!(validate: false)


  r2 = Recording.create!(name: "Parents backyard", attempts: 1, video_settings: vs)
  s3 = Schedule.new(start_at: Time.now - 2.week,
                    stop_at: Time.now + 3.days,
                    timezone: "Europe/Prague",
                    camera_id: 1,
                    recording: r2,
                    filters: [{ from: "10:00", to: "13:00" }])
  s3.save!(validate: false)

  end
end