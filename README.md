# README

Tlapse je aplikace pro Time-lapse videa, která je součástí Click2Stream Appstore.

### Config - přejmenovat si config soubory:

    cp config/database.yml.template config/database.yml
    cp config/redis_sidekiq.yml.template config/redis_sidekiq.yml
    cp config/sidekiq.yml.template config/sidekiq.yml
    cp config/redis_session.yml.template config/redis_session.yml
    cp config/application.yml.template config/application.yml

### Databáze - vytvořit z migrace

    rake db:migrate
    rake db:migrate RAILS_ENV=test

### Sidekiq

  - V pozadí běží sidekiq proces, který obstarává jak klasické úlohy tak scheduled. Pouští se:

        bundle exec sidekiq -C config/sidekiq.yml

  - stav jednotlivých jobů lze sledovat na adrese [http://localhost:3000/sidekiq] poté, co se spustí server:

        bin/rails s

### Click2Stream OAuth2

 **Toto musíte nastavit, jinak nefunguje žádná API komunikace s C2S**

 1. Do /etc/hosts přidejte řádek `127.0.0.1 timelapse-devel.6artisans.com`
 2. Nainstalujte bundle pod sudem `sudo bundle`
 3. Spusťte server pod sudem na portu 80 `sudo bin/rails s -p 80`
 4. V browseru otevřete `http://timelapse-devel.6artisans.com/`

 Alternativa je použít gem foreman a spustit `foreman start`, které spustí jak server, tak sidekiq

### Flowplayer - html5 přehrávač s flash fallbackem

  - relevantní linky
  - https://flowplayer.org/
  - https://flowplayer.org/docs/skinning.html
  - http://demos.flowplayer.org/lookandfeel/


### Click2Stream API

  - http://docs.click2stream.apiary.io (hlavní API pro práci s kamerama a uživatelem)
  - http://docs.click2streamsnapshotting.apiary.io/ (snapshotting API)
  - http://docs.click2streamapisnapshoter.apiary.io/ (verze snapshoteru, kterou ale nevyužíváme)
