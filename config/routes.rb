# == Route Map
#
#                         Prefix Verb  URI Pattern                                   Controller#Action
#                    sidekiq_web       /sidekiq                                      Sidekiq::Web
#             download_recording GET   /recordings/:id/download(.:format)            recordings#download
#             checkout_recording GET   /recordings/:id/checkout(.:format)            recordings#checkout
#                                PATCH /recordings/:id/checkout(.:format)            recordings#process_checkout
#           start_over_recording PATCH /recordings/:id/start_over(.:format)          recordings#start_over
#         choose_video_recording PATCH /recordings/:id/choose_video(.:format)        recordings#choose_video
#                     recordings GET   /recordings(.:format)                         recordings#index
#                                POST  /recordings(.:format)                         recordings#create
#                  new_recording GET   /recordings/new(.:format)                     recordings#new
#                 edit_recording GET   /recordings/:id/edit(.:format)                recordings#edit
#                      recording GET   /recordings/:id(.:format)                     recordings#show
#                                PATCH /recordings/:id(.:format)                     recordings#update
#                                PUT   /recordings/:id(.:format)                     recordings#update
#                  callback_auth GET   /auth/callback(.:format)                      auths#callback
#                    logout_auth GET   /auth/logout(.:format)                        auths#logout
#                       new_auth GET   /auth/new(.:format)                           auths#new
# all_your_base_are_belong_to_us GET   /all_your_base_are_belong_to_us(.:format)     all_your_base_are_belong_to_us#index
#  all_your_base_are_belong_to_u GET   /all_your_base_are_belong_to_us/:id(.:format) all_your_base_are_belong_to_us#show
#                timezone_camera GET   /cameras/:id/timezone(.:format)               cameras#timezone
#     detect_timezone_api_camera GET   /api/cameras/:id/detect_timezone(.:format)    api/cameras#detect_timezone
#                           root GET   /                                             pages#index
#

Rails.application.routes.draw do

  # Sidekiq web UI
  # TODO přesunoutasi na samostatnou appku, nebo do raketasku
  require 'sidekiq/web'
  mount Sidekiq::Web, at: '/sidekiq'

  resources :recordings do
    member do
      get  :download
      get  :checkout
      post :checkout, action: :process_checkout
      patch :choose_video
    end
  end

  resource :auth, only: [:new, :callback] do
    get :callback, on: :member
    get :logout, on: :member
  end

  resources :all_your_base_are_belong_to_us, only: [:index, :show]

  resources :cameras, only: [] do
    get :timezone, on: :member
  end

  namespace :api do
    resources :cameras, only: [] do
      member do
        get :detect_timezone
      end
    end
  end

  root to: "pages#index"
end
