# Allow to decode gziped/deflated response
require File.join(Rails.root, 'lib', 'faraday', 'gzip.rb')

Faraday::Response.register_middleware(gzip: Faraday::Response::Gzip)

class OAuth2::Client
  def connection
    @connection ||= begin
      conn = Faraday.new(site, options[:connection_opts])

      conn.response :gzip

      if options[:connection_build]
        conn.build do |b|
          options[:connection_build].call(b)
        end
      end

      conn
    end
  end
end