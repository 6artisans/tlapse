if defined?(MailSafe::Config) and Settings.respond_to? :override_mail_recipient and override = Settings.override_mail_recipient
  MailSafe::Config.internal_address_definition = /.*6artisans.*/i
  MailSafe::Config.replacement_address = override
end

# if Settings.respond_to? :override_mail_recipient and override = Settings.override_mail_recipient
#   class OverrideMailRecipient
#     def initialize(override)
#       @override = override
#     end

#     def delivering_email(mail)
#       mail.to = @override
#     end
#   end

#   ActionMailer::Base.register_interceptor(OverrideMailRecipient.new(override))
# end
