# Be sure to restart your server when you modify this file.

# Rails.application.config.session_store :cookie_store, key: '_tlapse_session'

Rails.application.config.session_store :redis_session_store, {
  key:        '_tlapse_session',
  serializer: :json,
  redis:      YAML.load_file("#{Rails.root}/config/redis_session.yml")[Rails.env].symbolize_keys
}