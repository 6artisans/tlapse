Rails.application.assets.logger = Logger.new('/dev/null')

Rails::Rack::Logger.class_eval do
  def is_banned_for_log?(env)
    (Rails.env.production? ? false : env['PATH_INFO'] =~ %r{^/assets/}) ||
    env['PATH_INFO'] =~ %r{^/sidekiq/\w+.*$} # Nechceme zobrazovat detail sidekiqu.
  end

  def call_with_quiet_assets(env)
    previous_level = Rails.logger.level
    Rails.logger.level = Logger::ERROR if is_banned_for_log?(env)
    call_without_quiet_assets(env)
  ensure
    Rails.logger.level = previous_level
  end

  alias_method_chain :call, :quiet_assets
end
