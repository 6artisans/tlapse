redis_config = YAML.load_file("#{Rails.root}/config/redis_sidekiq.yml")[Rails.env].symbolize_keys

module Sidekiq
  module Logging
    class Pretty < Logger::Formatter
      def call(severity, time, program_name, message)
        "#{Time.now.strftime("%Y-%m-%d %H:%M:%S %z")} PID::#{::Process.pid} TID::#{Thread.current.object_id.to_s(36)}#{context} #{severity}:: #{message}\n"
      end
    end
  end
end

Sidekiq.logger.formatter = Sidekiq::Logging::Pretty.new

Sidekiq.configure_server do |config|

  # Chceme aby se vše, co se odehraje během sidekiqu logovalo do sidekiq logu.
  Rails.logger              = Sidekiq.logger
  ActiveRecord::Base.logger = Sidekiq.logger
  Paperclip.logger          = Sidekiq.logger

  config.redis = redis_config
end

Sidekiq.configure_client do |config|
  config.redis = redis_config
end

Sidekiq.default_worker_options = { backtrace: 10, dead: true }
